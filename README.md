# Sudoku-Solver---c-sharp
13th grade sudoku solver written in c#
Enter sudoku as string of characters, as long as board is valid it is solved
provided unit tests testing if diffrent types of boards return the excpected outcome

Written in c# on visual studio 2019 (.net framework) A program that solves any sudoku board and can get input from a file or the console I used a backtracking algorithem with a few tweaks (using a list matrix to represent all possible options for each position on the board before starting the backtracking and running over each position in the list instead of all possible number from 1 to size) to solve the sudoku board.

Has 16 different unit tests checking different boards
